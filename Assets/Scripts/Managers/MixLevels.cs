﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class MixLevels : MonoBehaviour {

	public AudioMixer masterMixer;
	private float sfxvol;
	private float musicvol;
	void Awake()
	{
		masterMixer.GetFloat("sfxVol",out sfxvol);
		masterMixer.GetFloat("musicVol",out musicvol);
	}
	public void SetSfxLvl(float sfxLvl)
	{
		masterMixer.SetFloat ("sfxVol", sfxLvl);
	}
	public void SetMusicLvl(float musicLvl)
	{
		masterMixer.SetFloat ("musicVol", musicLvl);
	}
	public void SetAudioToggle()
	{
		if (sfxvol > -80f || musicvol > -80f) {
			masterMixer.SetFloat ("sfxVol", -80f);
			masterMixer.SetFloat ("musicVol", -80f);
		} else if(sfxvol == -80f && musicvol == -80f){
			masterMixer.SetFloat ("sfxVol", sfxvol);
			masterMixer.SetFloat ("musicVol", musicvol);
		}
	}
}